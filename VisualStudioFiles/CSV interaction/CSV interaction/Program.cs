﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSV_interaction
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteCsv(ReadCsv("..\\..\\testdata.csv"), "..\\..\\test2.csv");
        }

        static string[][] ReadCsv(string dir)
        {
            string[] rows = File.ReadAllLines(dir);

            List<string[]> data = new List<string[]>();

            foreach (string x in rows)
            {
                string[] n = x.Split(',');

                for (int i = 0; i < n.Length; i++)
                {
                    n[i] = n[i].Replace("\"","");
                }

                data.Add(n);
            }

            return data.Select(Enumerable.ToArray).ToArray();
        }

        static void WriteCsv(string[][] data, string dir)
        {
            if (! dir.EndsWith(".csv"))
            {
                dir = dir + ".csv";
            }

            string output = "";

            foreach (string[] row in data)
            {
                foreach (string x in row)
                {
                    string n = "";
                    if (x.Contains(","))
                    {
                        n = "\"" + x + "\"";
                    }
                    else
                    {
                        n = x;
                    }
                    output = output + n;
                    if (x != row.Last()) { output = output + ","; }
                }

                output = output + Environment.NewLine;
            }

            File.WriteAllText(dir, output);
        }
    }
}
