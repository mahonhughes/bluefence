﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Net;

namespace Blast_REST_API
{
    class Program
    {
        static void Main(string[] args)
        {
            string searchcode = "";
            const string api_url = "https://blast.ncbi.nlm.nih.gov/blast/Blast.cgi";

            using (WebClient client = new WebClient())
            {
                string response = client.DownloadString(api_url + "?QUERY=u00001&DATABASE=nt&PROGRAM=blastn&CMD=Put");
                string[] returnedData = Regex.Split(response, "QBlastInfoBegin");
                //Console.WriteLine(returnedData[1]);
                string[] usefulReturnedData = Regex.Split(returnedData[1], "RTOE");
                int position = usefulReturnedData[0].IndexOf("=") + 2;
                searchcode = usefulReturnedData[0].Substring(position, 11);
            }

            using (WebClient client = new WebClient())
            {
                int delay = 1;
                string[] usefulReturnedData = { "null", "null" };
                //string[] returnedData;
                string response;
                bool done = false;

                while (!done) {
                    Console.Clear();
                    response = client.DownloadString(api_url + "?CMD=Get&FORMAT_OBJECT=SearchInfo&RID=" + searchcode);
                    //if (response.Contains("Status=WAITING")

                    if (response.Contains("Status=FAILED") | response.Contains("Status=UNKNOWN"))
                    {
                        Console.WriteLine("Warning: code rejected");
                    }

                    if (response.Contains("Status=READY"))
                    {
                        done = true;
                    }

                    Thread.Sleep(delay);
                    delay = delay + delay;
                }
            }

            using (WebClient client = new WebClient())
            {
                string response = client.DownloadString(api_url + "?CMD=Get&FORMAT_TYPE=Text&RID=" + searchcode);
                Console.WriteLine(response);
                Console.ReadKey();
            }
        }
    }
}
